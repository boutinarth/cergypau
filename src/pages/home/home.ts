import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { LoadingController } from 'ionic-angular';
import { AutocompletePage } from '../../pages/autocomplete/autocomplete';
import {Observable} from 'rxjs/Observable';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})




export class HomePage {
  address;
  observableEtape;
  voitureListener;
  ionfocusfix=0;
  coordonnees ={
      latitude:0 ,
      longitude:0 
    };

  directionsService = new google.maps.DirectionsService;

  public db = firebase.firestore();

  constructor(private modalCtrl:ModalController,public loadingCtrl: LoadingController,public voyage:VoyageProvider,public voiture:VoitureProvider,private storage: Storage,private platform: Platform,private geolocation: Geolocation,public variable: VarProvider,public navCtrl: NavController, public locationTracker: LocationTrackerProvider) {
     this.address = {
      place: ''
    };
  }  

  showAddressModal () {
    //Fix tout pourri car ionfocus event fires twice
    this.ionfocusfix++;
    if (this.ionfocusfix<2)
    {
      //console.log("showadress modal function");
      let modal = this.modalCtrl.create(AutocompletePage);
      modal.onDidDismiss(data => {
        this.address.place = data;
        this.ionfocusfix=0;
      });
      modal.present();
      }
  }



  loadDistanceEtape() {
    // GERE SUR GOOGLE CLOUD FUNCTIONS
    // updatePosVoiture
    const idvoiture=this.variable.datavoiture.findIndex(i=> i.id === this.variable.voitureid);
    const voiture = this.variable.datavoiture[idvoiture];
    this.variable.etape.distance=voiture.etapeDistance;
    this.variable.etape.duree=voiture.etapeDuree;
    /*Update dans la Bdd les infos sur la voiture
    let distance;
    let duree;
    let infosTrajet;
    let polylinePath;
    console.log("this.variable.pos",JSON.stringify(this.variable.pos));
    console.log("this.variable.etape.pos",JSON.stringify(this.variable.etape.pos));
    this.directionsService.route({
      origin: this.variable.pos.latitude+" "+this.variable.pos.longitude,
      destination: this.variable.etape.pos.latitude+" "+this.variable.etape.pos.longitude,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        console.log("RESPONSE GOOGLE DIRECTIONS :",response);
        infosTrajet=response.routes[0].legs[0];
        duree=infosTrajet.duration.text;
        distance=infosTrajet.distance.text;
        this.variable.etape.distance=distance;
        this.variable.etape.duree=duree;
        this.variable.voitureupdate=firebase.firestore.Timestamp.now()
        //On update les donnees sur la mise à jour de la voiture toutes les 90secondes (par user)
        /*if(((firebase.firestore.Timestamp.now().seconds-this.variable.voitureupdate.seconds-90)>0)&&(this.variable.etape.encours))
        {
          //Géré sur Google Cloud Functions
          // On ne met a jour la vitesse que si elle est >1
          if((this.variable.vitesse>1)){
          let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid);
            return docRef.update({
            etapeDistance : parseInt(distance,10),
            etapeDuree : duree,
            update : this.variable.voitureupdate,
            pos: this.variable.pos,
            vitesse: this.variable.vitesse
          })
          }else{
              let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid);
              return docRef.update({
              etapeDistance : parseInt(distance,10),
              etapeDuree : duree,
              update : this.variable.voitureupdate,
              pos: this.variable.pos
            })
          }       
        } 
      } else {
        /*A virer
        window.alert('Directions request failed due to ' + status);
      }
    });*/
  }

  rejoindreVoiture(id): Promise<any>{
    return this.voiture.rejoindreVoiture(id).then((truc)=>{
      //this.loadDistanceEtape();
    });
  }


  deco()
  {
    this.storage.clear()
    .then((truc)=>{
      //Ferme le socket du listener
      this.voitureListener();
      return this.platform.exitApp();
    })
    .catch((err) =>{
      console.log("Erreur lors de la suppression du local storage",err);
    });   
  }

  ionViewWillEnter(){
    //Ugly fix pour que le ionfocus event ne fire qu'une fois et pas deux.
    this.ionfocusfix=0;
    //console.log("Dump this.varible",JSON.stringify(this.variable));
  }

  ionViewCanEnter(): Promise<any>{
      return Promise.resolve(true); 
  }

  ionViewDidLoad(){
    this.voitureListener=this.voiture.listenerColVoitures();
    this.voiture.dbuserhasvoiture();
    this.voyage.loadEtapeNul();//Load l'étape une fois
    this.observableEtape=this.voyage.loadEtapeListener(60);//Charge le listener qui va trigger toutes les X secondes
    if(this.variable.voitureid!=undefined){
      //Interval de mise a jour de la position sur firebase
      this.locationTracker.startTracking(100);     
      
      let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").doc(this.variable.userid);
      docRef.get() 
        .then((data)=>{
          if(data.exists){
            this.variable.isadmin=data.data().admin;
          }
        }).catch((err)=>{console.log("Erreur lors de la récupération depuis la bdd :",err)});
      }
    }
}