var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { LoadingController } from 'ionic-angular';
import { AutocompletePage } from '../../pages/autocomplete/autocomplete';
var HomePage = /** @class */ (function () {
    /*
      private aUneVoiture : boolean;
    private datavoiture = new Array();
    private voitureid;
    private voitureette;
    private membresvoiture;
    private nomnouvellevoiture: string;
    private username;
    private userid;*/
    function HomePage(modalCtrl, loadingCtrl, voyage, voiture, storage, platform, geolocation, variable, navCtrl, locationTracker) {
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.voyage = voyage;
        this.voiture = voiture;
        this.storage = storage;
        this.platform = platform;
        this.geolocation = geolocation;
        this.variable = variable;
        this.navCtrl = navCtrl;
        this.locationTracker = locationTracker;
        this.ionfocusfix = 0;
        this.coordonnees = {
            latitude: 0,
            longitude: 0
        };
        this.directionsService = new google.maps.DirectionsService;
        this.db = firebase.firestore();
        this.address = {
            place: ''
        };
    }
    HomePage.prototype.showAddressModal = function () {
        var _this = this;
        //Fix tout pourri car ionfocus event fires twice
        this.ionfocusfix++;
        if (this.ionfocusfix < 2) {
            console.log("showadress modal function");
            var modal = this.modalCtrl.create(AutocompletePage);
            modal.onDidDismiss(function (data) {
                _this.address.place = data;
                _this.ionfocusfix = 0;
            });
            modal.present();
        }
    };
    HomePage.prototype.calculDistanceEtape = function () {
        var _this = this;
        var distance;
        var duree;
        var infosTrajet;
        this.directionsService.route({
            origin: this.variable.pos.latitude + " " + this.variable.pos.longitude,
            destination: this.variable.etape.pos.latitude + " " + this.variable.etape.pos.longitude,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                console.log("RESPONSE GOOGLE DIRECTIONS :", response);
                infosTrajet = response.routes[0].legs[0];
                duree = infosTrajet.duration.text;
                distance = infosTrajet.distance.text;
                _this.variable.etape.distance = distance;
                _this.variable.etape.duree = duree;
                var docRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("voitures").doc(_this.variable.voitureid);
                return docRef.update({
                    etapeDistance: parseInt(distance, 10),
                    etapeDuree: duree,
                    etapeUpdate: firebase.firestore.Timestamp.now()
                });
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    };
    HomePage.prototype.rejoindreVoiture = function (nom) {
        return this.voiture.rejoindreVoiture(nom)
            .catch(function (err) {
            console.log("Erreur lors de updateshit()", err);
        });
    };
    HomePage.prototype.deco = function () {
        var _this = this;
        this.storage.clear()
            .then(function (truc) {
            return _this.platform.exitApp();
        })
            .catch(function (err) {
            console.log("Erreur lors de la suppression du local storage", err);
        });
    };
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.ionfocusfix = 0;
        if (this.variable.voitureid != undefined) {
            var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").doc(this.variable.userid);
            docRef.get()
                .then(function (data) {
                if (data.exists) {
                    _this.variable.isadmin = data.data().admin;
                }
            }).catch(function (err) { console.log("Erreur lors de la récupération depuis la bdd :", err); });
        }
        this.voyage.loadEtape();
        if (this.variable.etape.encours) {
            this.calculDistanceEtape();
        }
    };
    HomePage.prototype.ionViewCanEnter = function () {
        console.log('ionViewCanEnter HomePage');
        console.log('dump de this.variable1', this.variable);
        //return Promise.all([this.voiture.dbuserhasvoiture(),this.voiture.getvoitures()]).then((truc)=>{
        //Quand la promesse users ET voiture est résolue
        return Promise.resolve(true);
        //return Promise.resolve(this.variable.voitureette=this.voiture.dbgetvoiturebylocalid(this.variable.voitureid,this.variable.datavoiture));
        //})
    };
    HomePage.prototype.ionViewDidLoad = function () {
        if (this.variable.voitureid != undefined) {
            this.locationTracker.startTracking();
        }
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [ModalController, LoadingController, VoyageProvider, VoitureProvider, Storage, Platform, Geolocation, VarProvider, NavController, LocationTrackerProvider])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map