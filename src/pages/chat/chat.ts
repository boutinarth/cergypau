import { Component, ViewChild , ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { VarProvider } from '../../providers/var/var';
import { Content, List } from 'ionic-angular';
/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
	contenuMessage:string;
	@ViewChild(Content) content: Content;
	@ViewChild(List, {read: ElementRef}) chatList: ElementRef;
	private mutationObserver: MutationObserver;

  constructor(public variable:VarProvider,public chat:ChatProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  sendMessage(contenu): Promise<any>{
  	let nouveauMsg={
  		message:contenu,
  		nom:this.variable.loginName,
  		photo:this.variable.photoUrl};
  		return this.chat.sendMessage(nouveauMsg)
  		.then((truc)=>{
				return this.contenuMessage='';
  		})
  		.catch((err)=>{
  			console.log("Erreur lors de l'ajout d'un message :", err );
  		})
  }

  deleteMessage(id){
  	return this.chat.deleteMessage(id);
  }

   ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');

    //listener pour scroll automatiquement vers le bas lors d'un message
    this.mutationObserver = new MutationObserver((mutations) => {
    		console.log(mutations);
    		mutations.forEach((changement)=>{
    			if(changement.addedNodes.length>0){
				this.content.scrollToBottom();
    			}
    		})
        });
 
        this.mutationObserver.observe(this.chatList.nativeElement, {
            childList: true
        });
  }

  ionViewWillEnter(): void {
    this.chat.listener();
    }

  ionViewDidLeave(){
    //Pour réduire la consomation de données on se désabonne des messages lorsqu'on quite la page.
    this.chat.unsubscribe();
  }

}
