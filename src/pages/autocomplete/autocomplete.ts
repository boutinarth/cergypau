import {Component, NgZone, Input, ViewChild} from '@angular/core';
import {ViewController} from 'ionic-angular';
import {NavController} from 'ionic-angular';

declare var google;

@Component({
  templateUrl: 'autocomplete.html'
})

export class AutocompletePage {
	@ViewChild('searchbar') searchbar ;

  autocompleteItems;
  autocomplete;

  latitude: number = 0;
  longitude: number = 0;
  geo: any

  service = new google.maps.places.AutocompleteService();

  constructor (public viewCtrl: ViewController, private zone: NgZone) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
    this.geo = item;
    //this.geoCode(this.geo);//convert Address to lat and long
  }

  updateSearch() {
  	console.log("autocomplete.ts query =",this.autocomplete.query);

    if (this.autocomplete.query == '') {
     this.autocompleteItems = [];
     return;
    }

    let me = this;
    this.service.getPlacePredictions({
    input: this.autocomplete.query,
    componentRestrictions: {
      country: 'fr'
    }
   }, (predictions, status) => {
     me.autocompleteItems = [];

   me.zone.run(() => {
     if (predictions != null) {
        predictions.forEach((prediction) => {
          me.autocompleteItems.push(prediction.description);
        });
       }
     });
   });
  }

  ionViewDidEnter(){
  	setTimeout(() => {
      this.zone.run(()=>{
        this.searchbar.setFocus();
      })
    }, 100)
  }

}