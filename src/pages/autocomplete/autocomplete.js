var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone, ViewChild } from '@angular/core';
import { ViewController } from 'ionic-angular';
var AutocompletePage = /** @class */ (function () {
    function AutocompletePage(viewCtrl, zone) {
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.latitude = 0;
        this.longitude = 0;
        this.service = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
    }
    AutocompletePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AutocompletePage.prototype.chooseItem = function (item) {
        this.viewCtrl.dismiss(item);
        this.geo = item;
        //this.geoCode(this.geo);//convert Address to lat and long
    };
    AutocompletePage.prototype.updateSearch = function () {
        console.log("autocomplete.ts query =", this.autocomplete.query);
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({
            input: this.autocomplete.query,
            componentRestrictions: {
                country: 'fr'
            }
        }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                if (predictions != null) {
                    predictions.forEach(function (prediction) {
                        me.autocompleteItems.push(prediction.description);
                    });
                }
            });
        });
    };
    AutocompletePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.zone.run(function () {
                _this.searchbar.setFocus();
            });
        }, 100);
    };
    __decorate([
        ViewChild('searchbar'),
        __metadata("design:type", Object)
    ], AutocompletePage.prototype, "searchbar", void 0);
    AutocompletePage = __decorate([
        Component({
            templateUrl: 'autocomplete.html'
        }),
        __metadata("design:paramtypes", [ViewController, NgZone])
    ], AutocompletePage);
    return AutocompletePage;
}());
export { AutocompletePage };
//# sourceMappingURL=autocomplete.js.map