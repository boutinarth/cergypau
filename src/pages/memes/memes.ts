import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { HttpClient } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { NativeAudio } from '@ionic-native/native-audio';

/**
 * Generated class for the MemesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-memes',
  templateUrl: 'memes.html',
})
export class MemesPage {
	nbActions:number;
	ajoutencours=false;
	titreVideo:string;
	alertmessage:string;
	APIkey="AIzaSyDqw1mrpuLAtkSpapN6m854s7ZP1d6mIfo";
	riotAPIkey="AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8";
	response;
	url;
	public db = firebase.firestore();
	//soundbox=[];//array of Element : {first : {nom,id}, second : {nom,id}}
	soundbox=[{first:{nom:"Soundbox1",id:1},second:{nom:"Soundbox2",id:2}},
	{first:{nom:"Soundbox3",id:3},second:{nom:"Soundbox4",id:4}},
	{first:{nom:"Soundbox5",id:5},second:{nom:"Soundbox6",id:6}},
	{first:{nom:"Soundbox7",id:7},second:{nom:"Soundbox8",id:8}}
	];

  constructor(private nativeAudio: NativeAudio,private youtube: YoutubeVideoPlayer, private iab: InAppBrowser,private alertCtrl: AlertController,public http: HttpClient,public variable:VarProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MemesPage');
    //Nombre d'actions sur la soundbox
    this.nbActions=10;
    if(this.variable.isadmin){
    	this.listenerColSoundbox();	
    	this.nbActions=100;
    }
  }

  	playVideos(videos :any){
  		let video = videos.shift();
  		//console.log("video id ",video.id);
  		//console.log('video duration', video.duree);
  		this.youtube.openVideo(video.id);
  		let that = this;
  		setTimeout(function(){ 
  			that.playVideos(videos);
       }, (video.duree+10)*1000);  
  	}

  	//Listener Firebase pour la collection SoundBox
  listenerColSoundbox(){
  	let time =firebase.firestore.Timestamp.now();
  	//Load the soundbox
  	for (var i = 1; i < 5; ++i) {
  		this.nativeAudio.preloadSimple( i+'' , 'assets/sounds/'+ i +'.wav');	
  	}
  	const ref=this.db.collection("voyages").doc(this.variable.voyageid).collection("Soundbox");
  	return ref.onSnapshot((array)=>{
  		array.docChanges().forEach((change)=> {
  		 	let data=change.doc.data();
  		 	let sound=data;
           	sound.id=Number(data.soundid);
            if (change.type === "added" && (time.seconds<sound.dateAjout.seconds)) {
            	//Si un son est ajouté on le joue
              	this.nativeAudio.play(sound.id);
                console.log("Son voiture: ", JSON.stringify(data));
            }
        });
  	})
  }

   playSoundbox(id:number){
  	if(this.nbActions>0){
  		this.nbActions--;
  		let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("Soundbox").doc();
	  		let setDoc = docRef.set({
	  			soundid : id,
	  			dateAjout : firebase.firestore.Timestamp.now()
	  		});
  	}else{
  		window.alert("Tu n'as plus d'actions disponibles.");
  	}
  }


  

	createPlaylist(){
		let videos = new Array();
		this.url="https://www.youtube.com/watch_videos?video_ids=";
		let colref=this.db.collection("voyages").doc(this.variable.voyageid).collection("playlist");
		colref.get().then((snapshot)=>{
				snapshot.forEach((doc) => {
		        let data = doc.data();
		        this.url=this.url+","+data.id;
		        videos.push(data);
	    	});
			//let browser = this.iab.create(this.url);
			console.log(videos);
			this.playVideos(videos);
			/*videos.forEach((video)=>{
				this.youtube.openVideo(video.id);
			})*/
		});
	}

  ajouterVideo(titre : string){
  	this.ajoutencours=true;
  	if(this.nbActions>0)
  	{
  	//Requete youtube API : 1 resultat, que des videos.
  	titre=titre.replace(" ","+");
  	let requete="https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q="+titre+"&type=video&key="+this.APIkey+"&type=video";
  	console.log("requete : ",requete);
  	let http=this.http.get(requete)
  	.subscribe((data) => {
  		this.response = data;
  		console.log(data);
  		if(this.response.pageInfo.totalResults>0){
	  		let video = { 	id : this.response.items[0].id.videoId,
	  					title : this.response.items[0].snippet.title,
	  					thumbnail : this.response.items[0].snippet.thumbnails.default.url};
	  		let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("playlist").doc();
	  		let setDoc = docRef.set({
	  			userid : this.variable.userid,
	  			title : video.title,
	  			id : video.id,
	  			thumbnail : video.thumbnail,
	  			dateAjout : firebase.firestore.Timestamp.now()
	  		});
	  		//this.presentAlert("Vidéo ajoutée",this.alertmessage);
	  		//2ème requête necessaire pour avoir la durée de la vidéo
	  		setDoc
	  		.then((truc)=>{
	  			this.nbActions--;
	  			this.titreVideo="";
	  			this.alertmessage="";
	  			this.presentAlert("Vidéo ajoutée",this.alertmessage);
	  			let requete ="https://www.googleapis.com/youtube/v3/videos?id=" + video.id + "&part=contentDetails&key="+this.APIkey ;
	  			let http=this.http.get(requete)
			  	.subscribe((data) => {
			  		let response:any;
			  		response=data;
			  		let videoLength= response.items[0].contentDetails.duration;
			  		//Converti la chaine en secondes
			  		videoLength=videoLength.replace("PT","").replace("M","*60+").replace("S","*1");
			  		videoLength=eval(videoLength);
			  		docRef.update({
			  			duree : videoLength
			  		})
			  	});
	  		})
	  	}else{
	  		this.alertmessage="Veuillez réessayer.";
	  		this.presentAlert("Vidéo introuvable",this.alertmessage);
	  	}

	  	},(err)=>{ 
	  		//console.log("Erreur lors de la recherche de la video : ", err );
	  		this.alertmessage="Connexion perdue.";
	  		this.presentAlert("Erreur",this.alertmessage);
	  	});
	}else{
		window.alert("Tu n'as plus d'actions disponibles.");
	}
	//this.createPlaylist();
	this.ajoutencours=false;
	}



	presentAlert(title : string,message : string) {
	  let alert = this.alertCtrl.create({
	    title: title,
	    subTitle: message,
	    buttons: ['Ok']
	  });
	  alert.present();
	}


  

}
