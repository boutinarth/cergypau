import { Component} from '@angular/core';
import { IonicPage, NavController ,App} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { VoyageProvider } from '../../providers/voyage/voyage';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

   private position : firebase.firestore.GeoPoint;
  constructor(public voyage: VoyageProvider,public app: App,private geolocation: Geolocation, public variable: VarProvider) {
  }

  login(){
    this.geolocation.getCurrentPosition().then((resp)=>{
      return this.position=new firebase.firestore.GeoPoint(resp.coords.latitude,resp.coords.longitude);
    }).then((machin)=>{
      this.voyage.facebookLogin().then((truc)=>{
       console.log("6 variable.loginName avant redirection: ",JSON.stringify(this.variable.loginName));
       //redirection vers une page pour rejoindre / créer un voyage
       if(this.variable.loginState){
       this.app.getRootNav().setRoot(TabsPage);
       }else{
         window.alert("Une erreur est survenue lors de la connexion veuillez réessayer plus tard.");
       }
     })
    })
  }  

  loginPC(){
    this.voyage.facebookLogin().then((truc)=>{
       console.log("6 variable.loginName avant redirection: ",JSON.stringify(this.variable.loginName));
       //redirection vers une page pour rejoindre / créer un voyage
       if(this.variable.loginState){
       this.app.getRootNav().setRoot(TabsPage);
       }else{
         window.alert("Une erreur est survenue lors de la connexion veuillez réessayer plus tard.");
       }
     }).then((truc)=>{
        this.app.getRootNav().setRoot(TabsPage);
     })
  }

  ionViewWillEnter(): void {
  }

  ionViewCanEnter(){
      
 }
}