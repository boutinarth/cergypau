var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { VoyageProvider } from '../../providers/voyage/voyage';
var LoginPage = /** @class */ (function () {
    function LoginPage(voyage, app, geolocation, variable) {
        this.voyage = voyage;
        this.app = app;
        this.geolocation = geolocation;
        this.variable = variable;
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            return _this.position = new firebase.firestore.GeoPoint(resp.coords.latitude, resp.coords.longitude);
        }).then(function (machin) {
            _this.voyage.facebookLogin().then(function (truc) {
                console.log("6 variable.loginName avant redirection: ", JSON.stringify(_this.variable.loginName));
                //redirection vers une page pour rejoindre / créer un voyage
                _this.app.getRootNav().setRoot(TabsPage);
                //this.navCtrl.push(TabsPage);
            });
        });
    };
    LoginPage.prototype.ionViewWillEnter = function () {
    };
    LoginPage.prototype.ionViewCanEnter = function () {
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html'
        }),
        __metadata("design:paramtypes", [VoyageProvider, App, Geolocation, VarProvider])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map