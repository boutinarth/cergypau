var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { VarProvider } from '../../providers/var/var';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { VoitureProvider } from '../../providers/voiture/voiture';
/**
 * Generated class for the LoadingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoadingPage = /** @class */ (function () {
    function LoadingPage(voiture, voyage, app, variable, navCtrl, navParams, storage) {
        this.voiture = voiture;
        this.voyage = voyage;
        this.app = app;
        this.variable = variable;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
    }
    LoadingPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad LoadingPage');
        console.log("dump this.variable", this.variable);
        //On vérifie si l'utilisateur est logged in, si c'est le cas on set la rootPage à LoginPage sinon on la set à TabsPage
        this.storage.get('loginFb')
            .then(function (data) {
            console.log(".1 this.loginFb après storage.get:", data);
            return Promise.resolve(_this.loginFb = data);
        })
            /*.then((truc)=>{
               console.log(".2 this.variable.loginState après storage.get:",this.variable.loginState);
               console.log("var retour de la promise 0.2 : ", truc)
             if(this.variable.loginState){
               return this.storage.get('loginName');
               }
             })*/
            .then(function (data) {
            //Si l'utilisateur s'est déjà connecté une fois par Facebook à notre application
            if (_this.loginFb) {
                return _this.voyage.facebookLogin()
                    .then(function (truc) {
                    console.log('dump de this.variable page loading 1', _this.variable);
                    return _this.app.getRootNav().setRoot(TabsPage);
                });
                //this.navCtrl.push(TabsPage);
                //return Promise.reject("Déjà logged in");
            }
            else {
                console.log('dump de this.variable page loading 2', _this.variable);
                return _this.app.getRootNav().setRoot(LoginPage);
                //this.navCtrl.push(LoginPage);
            }
        }).catch(function (error) {
            console.log("Erreur page loading :", error);
        });
    };
    LoadingPage.prototype.ionViewWillLeave = function () {
        console.log("DUMP this.variable ionviewwillleaveLoading", this.variable);
    };
    LoadingPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-loading',
            templateUrl: 'loading.html',
        }),
        __metadata("design:paramtypes", [VoitureProvider, VoyageProvider, App, VarProvider, NavController, NavParams, Storage])
    ], LoadingPage);
    return LoadingPage;
}());
export { LoadingPage };
//# sourceMappingURL=loading.js.map