import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { VarProvider } from '../../providers/var/var';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { VoitureProvider } from '../../providers/voiture/voiture';
/**
 * Generated class for the LoadingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html',
})
export class LoadingPage {

  constructor(public voiture:VoitureProvider,public voyage: VoyageProvider,public app: App,public variable : VarProvider,public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {
  }

  loginFb:boolean;

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoadingPage');
    console.log("dump this.variable",this.variable);
    //On vérifie si l'utilisateur est logged in, si c'est le cas on set la rootPage à LoginPage sinon on la set à TabsPage
      this.storage.get('loginFb')
   .then(data => {
   	 console.log(".1 this.loginFb après storage.get:",data);
    return Promise.resolve(	this.loginFb=data);
    })
    .then(data => {
      //Si l'utilisateur s'est déjà connecté une fois par Facebook à notre application
      if(this.loginFb){
      	return this.voyage.facebookLogin()
      		.then((truc)=>{      		
      			console.log('dump de this.variable page loading 1',this.variable);
				return this.app.getRootNav().setRoot(TabsPage);
      			})
      }
      else{
      		console.log('dump de this.variable page loading 2',this.variable);
			return this.app.getRootNav().setRoot(LoginPage);
      }      
    }).catch((error)=>{
      console.log("Erreur page loading :",error)
    });
  }

  ionViewWillLeave(){
  	console.log("DUMP this.variable ionviewwillleaveLoading",this.variable);
  }

}
