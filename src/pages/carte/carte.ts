import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Encoding,
  ILatLng
} from '@ionic-native/google-maps';
import { VarProvider } from '../../providers/var/var';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage'
import firebase from 'firebase';


declare var google;

/**
 * Generated class for the CartePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-carte',
  templateUrl: 'carte.html',
})
export class CartePage {
	map: GoogleMap;
  marqueurs=new Array();
  marqueurEtape:Marker;
  classementVoitures= new Array();
  directionsService = new google.maps.DirectionsService;

  constructor(public voyage: VoyageProvider,public voiture: VoitureProvider,public variable:VarProvider,public navCtrl: NavController, public navParams: NavParams) {
  }
  db = firebase.firestore();

  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
         target: {
           lat: this.variable.pos.latitude,
           lng: this.variable.pos.longitude
         },
         zoom: 8,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map_canvas', mapOptions);   
  }

  ajouterPolyline() : any{
    //Trace l'étape sur la map
    console.log("ajouterPolyline()0");
    this.directionsService.route({
      origin: this.variable.etape.depart.latitude+" "+this.variable.etape.depart.longitude,
      destination: this.variable.etape.pos.latitude+" "+this.variable.etape.pos.longitude,
      travelMode: 'DRIVING'
    }, (response, status) => {
      console.log("ajouterPolyline()1");
      if (status === 'OK') {
        console.log("ajouterPolyline()2");
        console.log("RESPONSE GOOGLE DIRECTIONS :",response);
        let encodedpath= response.routes[0].overview_polyline;
        let points: ILatLng[] = Encoding.decodePath(encodedpath);
        let polylinePath=response.routes[0].overview_path;
        this.map.addPolyline({
                points: points,
                width:4,
                color: "blue"
            });
      } else {
        window.alert('Ajout de la polyline a échoué : ' + status);
      }
      console.log("ajouterPolyline()4");
    });
  }

  //Ajoute la position des voitures sur la carte.
  ajouterPosVoitures() : any
  {
    //console.log("this.marqueurs début fonction :",this.marqueurs);
    //Supprime les marqueurs déjà existants pour ne pas les avoir en double.
     for (var i = 0; i < this.marqueurs.length; i++) {
          this.marqueurs[i].remove();
        }
    this.marqueurs=new Array();

      //console.log("dump this.variable.voiture avant la création des marqueurs",this.variable.datavoiture);
  		this.variable.datavoiture.forEach((voiture) => {
  			//console.log("dump voiture lors de la création du marqueur.",voiture);
             //Une voiture venant d'être créée n'a pas de position on vérifie avant de l'ajouter sur la carte
             if(voiture.pos!=undefined){
        		let marker: any = this.map.addMarkerSync({
				      title: voiture.nom,
				      icon: "",
				      position: { lat:voiture.pos.latitude, lng:voiture.pos.longitude }
			   		 });
            //this.marqueursnom.push(voiture.nom);
            this.marqueurs.push(marker);
          }
		  });
       console.log("this.marqueurs",this.marqueurs);
	}

  ajouterPosEtape() : any{
    console.log("dump this.variable.etape",this.variable.etape);
    //On supprime le marqueur étape s'il existe avant de le refresh
    if(this.marqueurEtape != undefined){
      this.marqueurEtape.remove();
    }
      if(this.variable.etape.encours){
        this.marqueurEtape=this.map.addMarkerSync({
              position: { lat:this.variable.etape.pos.latitude, lng:this.variable.etape.pos.longitude },
              title: "Etape : "+this.variable.etape.nom,
              icon: {
                      url: "assets/imgs/end_flag.png",
                      size: {
                        width: 35,
                        height: 45
                      },
                      anchor: new google.maps.Point(6, 44)
                    }
              });
      }
  }

  ionViewWillLoad(){
    this.loadMap();
  };

	ionViewDidLoad() {
    let that = this;
    console.log('ionViewDidLoad CartePage');

    setTimeout(function(){ 
    that.voiture.notifier.subscribe((value) => { 
       console.log("Observable change :",value);
       that.ajouterPosVoitures();
      });

    that.voyage.Etapenotifier.subscribe((value) => { 
       console.log("Observable change Etapenotifier :",value);
       that.ajouterPolyline();
      });
       }, 3000);  
  }

  ionViewWillEnter() {
      this.ajouterPosVoitures();
      this.ajouterPosEtape();
  }
}
