var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GoogleMaps } from '@ionic-native/google-maps';
import { VarProvider } from '../../providers/var/var';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage';
import firebase from 'firebase';
/**
 * Generated class for the CartePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CartePage = /** @class */ (function () {
    function CartePage(voyage, voiture, variable, navCtrl, navParams) {
        this.voyage = voyage;
        this.voiture = voiture;
        this.variable = variable;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.marqueurs = new Array();
        this.classementVoitures = new Array();
        this.db = firebase.firestore();
    }
    CartePage.prototype.loadMap = function () {
        var mapOptions = {
            camera: {
                target: {
                    lat: this.variable.pos.latitude,
                    lng: this.variable.pos.longitude
                },
                zoom: 8,
                tilt: 30
            }
        };
        this.map = GoogleMaps.create('map_canvas', mapOptions);
    };
    //Ajoute la position des voitures sur la carte.
    CartePage.prototype.ajouterPosVoitures = function () {
        var _this = this;
        console.log("this.marqueurs début fonction :", this.marqueurs);
        //Supprime les marqueurs déjà existants pour ne pas les avoir en double.
        for (var i = 0; i < this.marqueurs.length; i++) {
            this.marqueurs[i].remove();
        }
        this.marqueurs = new Array();
        this.classementVoitures = new Array();
        console.log("dump this.variable.voiture avant la création des marqueurs", this.variable.datavoiture);
        this.variable.datavoiture.forEach(function (voiture) {
            console.log("dump voiture lors de la création du marqueur.", voiture);
            //On récupère le membre dont la position a été mise à jour la plus recemment.
            var docRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("voitures").doc(voiture.id).collection("users").orderBy('lastupdate', 'desc').limit(1);
            return docRef.get()
                .then(function (data) {
                console.log("dump data", data);
                //ForEach ne s'effectue qu'une seule fois car on prend uniquement un résultat depuis firebase : limit(1)
                data.forEach(function (row) {
                    console.log("dump row lors de la création du marqueur.", row.data());
                    var user = row.data();
                    var marker = _this.map.addMarkerSync({
                        title: voiture.nom,
                        icon: "",
                        animation: 'DROP',
                        position: { lat: user.pos.latitude, lng: user.pos.longitude }
                    });
                    _this.marqueurs.push(marker);
                    console.log("this.marqueurs dans la boucle:", _this.marqueurs);
                });
            })
                .catch(function (err) { console.log("Erreur ! ", err); });
        });
        console.log("dump classement voitures", this.classementVoitures);
    };
    CartePage.prototype.getClassementVoitures = function () {
        var _this = this;
        this.classementVoitures = new Array();
        //Logique classement voitures
        var docRefVoiture = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").orderBy("etapeDistance");
        docRefVoiture.get()
            .then(function (data) {
            data.forEach(function (rowvoiture) {
                var donnees = rowvoiture.data();
                var voiture = { nom: donnees.nom, distance: donnees.etapeDistance };
                _this.classementVoitures.push(voiture);
            });
        });
    };
    CartePage.prototype.ajouterPosEtape = function () {
        var _this = this;
        console.log("dump this.variable.etape", this.variable.etape);
        //On supprime le marqueur étape s'il existe avant de le refresh
        if (this.marqueurEtape != undefined) {
            this.marqueurEtape.remove();
        }
        this.voyage.loadEtape()
            .then(function (truc) {
            if (_this.variable.etape.encours) {
                _this.marqueurEtape = _this.map.addMarkerSync({
                    position: { lat: _this.variable.etape.pos.latitude, lng: _this.variable.etape.pos.longitude },
                    title: "Etape : " + _this.variable.etape.nom,
                    icon: {
                        url: "assets/imgs/end_flag.png",
                        size: {
                            width: 35,
                            height: 45
                        },
                        anchor: new google.maps.Point(6, 44)
                    },
                    animation: 'DROP'
                });
            }
        })
            .catch(function (err) { console.log("Erreur lors de la récupération de l'étape", err); });
    };
    CartePage.prototype.ionViewWillLoad = function () {
        this.loadMap();
    };
    ;
    CartePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CartePage');
    };
    CartePage.prototype.ionViewWillEnter = function () {
        this.ajouterPosVoitures();
        this.ajouterPosEtape();
    };
    CartePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-carte',
            templateUrl: 'carte.html',
        }),
        __metadata("design:paramtypes", [VoyageProvider, VoitureProvider, VarProvider, NavController, NavParams])
    ], CartePage);
    return CartePage;
}());
export { CartePage };
//# sourceMappingURL=carte.js.map