import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { CartePage } from '../carte/carte';
import { VarProvider } from '../../providers/var/var';
import { AdminPage } from '../admin/admin';
import { ChatPage } from '../chat/chat';
import { MemesPage } from '../memes/memes';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ChatPage;
  tab3Root = MemesPage;
  tab4Root = CartePage;
  tab5Root = AdminPage;

  constructor(public variable:VarProvider) {

  }
}
