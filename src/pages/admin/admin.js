var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { LoadingController } from 'ionic-angular';
import { AutocompletePage } from '../../pages/autocomplete/autocomplete';
/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdminPage = /** @class */ (function () {
    function AdminPage(modalCtrl, loadingCtrl, voyage, voiture, storage, platform, geolocation, variable, navCtrl, locationTracker) {
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.voyage = voyage;
        this.voiture = voiture;
        this.storage = storage;
        this.platform = platform;
        this.geolocation = geolocation;
        this.variable = variable;
        this.navCtrl = navCtrl;
        this.locationTracker = locationTracker;
        this.ionfocusfix = 0;
        this.coordonnees = {
            latitude: 0,
            longitude: 0
        };
        this.directionsService = new google.maps.DirectionsService;
        this.db = firebase.firestore();
        this.address = {
            place: ''
        };
    }
    AdminPage.prototype.showAddressModal = function () {
        var _this = this;
        //Fix tout pourri car ionfocus event fires twice
        this.ionfocusfix++;
        if (this.ionfocusfix < 2) {
            console.log("showadress modal function");
            var modal = this.modalCtrl.create(AutocompletePage);
            modal.onDidDismiss(function (data) {
                _this.address.place = data;
                _this.ionfocusfix = 0;
            });
            modal.present();
        }
    };
    AdminPage.prototype.deleteetape = function () {
        var _this = this;
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").doc(this.variable.etape.id);
        return docRef.get()
            .then(function (data) {
            if (data.exists) {
                _this.variable.etape = {
                    id: "",
                    nom: "",
                    pos: new firebase.firestore.GeoPoint(0, 0),
                    update: firebase.firestore.Timestamp.now(),
                    encours: false,
                    duree: "",
                    distance: ""
                };
                //On utilise set et non update car on veut une seule étape à chaque fois, on écrase le reste
                return docRef.update({
                    encours: false
                });
            }
            else {
                console.log("Erreur, cela ne devrait jamais se produire. Dump docRef :", docRef);
            }
        }).catch(function (err) { console.log("Erreur lors de la suppression de letape :", err); });
    };
    AdminPage.prototype.ajouterEtape = function () {
        var _this = this;
        console.log("début fonction ajouterEtape");
        console.log("valeur this.address", this.address);
        //convert place to geocode
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': this.address.place }, function (results, status) {
            if (status === 'OK') {
                _this.coordonnees.latitude = results[0].geometry.location.lat();
                _this.coordonnees.longitude = results[0].geometry.location.lng();
                //Ajout dans firebase
                var docRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("etapes").doc();
                console.log("docRef =", docRef);
                var coordonnees = new firebase.firestore.GeoPoint(_this.coordonnees.latitude, _this.coordonnees.longitude);
                var timestampupdate = firebase.firestore.Timestamp.now();
                //On utilise set et non update car on veut une seule étape à chaque fois, on écrase le reste
                _this.variable.etape.nom = _this.address.place;
                _this.variable.etape.pos = coordonnees;
                _this.variable.etape.update = timestampupdate;
                _this.variable.etape.encours = true;
                _this.variable.etape.id = docRef.id;
                return docRef.set({
                    pos: coordonnees,
                    nom: _this.address.place,
                    update: timestampupdate,
                    encours: true
                })
                    .then(function (truc) {
                    setTimeout(function (truc) {
                        _this.address.place = "";
                        // this.calculDistanceEtape(); 
                        console.log("Fin fonction ajouterEtape");
                    }, 150);
                })
                    .catch(function (err) { console.log("Erreur lors de l'ajout de l'étape :", err); });
            }
            else {
                console.log("Une erreur est survenue lors de la conversion", status);
            }
        });
    };
    AdminPage.prototype.creerVoiture = function (nomvoiture) {
        this.voiture.creerVoiture(nomvoiture);
    };
    AdminPage.prototype.ajouterAdmin = function (nom) {
        var _this = this;
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").where("nom", "==", nom);
        return docRef.get()
            .then(function (data) {
            data.forEach(function (user) {
                var docRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("voitures").doc(_this.variable.voitureid).collection("users").doc(user.id);
                return docRef.update({
                    admin: true
                });
            });
        }).catch(function (err) { console.log("Erreur lors de l'actualisation du statut admin :", err); });
    };
    AdminPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.ionfocusfix = 0;
        if (this.variable.voitureid != undefined) {
            var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").doc(this.variable.userid);
            docRef.get()
                .then(function (data) {
                if (data.exists) {
                    _this.variable.isadmin = data.data().admin;
                }
            }).catch(function (err) { console.log("Erreur lors de la récupération depuis la bdd :", err); });
        }
        this.voyage.loadEtape();
    };
    AdminPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AdminPage');
    };
    AdminPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-admin',
            templateUrl: 'admin.html',
        }),
        __metadata("design:paramtypes", [ModalController, LoadingController, VoyageProvider, VoitureProvider, Storage, Platform, Geolocation, VarProvider, NavController, LocationTrackerProvider])
    ], AdminPage);
    return AdminPage;
}());
export { AdminPage };
//# sourceMappingURL=admin.js.map