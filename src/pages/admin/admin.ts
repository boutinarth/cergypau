import { IonicPage, NavParams } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { VarProvider } from '../../providers/var/var';
import firebase from 'firebase';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';
import { LocationTrackerProvider } from '../../providers/location-tracker/location-tracker';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { VoyageProvider } from '../../providers/voyage/voyage';
import { LoadingController } from 'ionic-angular';
import { AutocompletePage } from '../../pages/autocomplete/autocomplete';

declare var google;


/**
 * Generated class for the AdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})


export class AdminPage {

	address;
  nomadmin;
  nomnouvellevoiture;
  ionfocusfix=0;
  coordonnees ={
      latitude:0 ,
      longitude:0 
    };

  directionsService = new google.maps.DirectionsService;

  public db = firebase.firestore();

  constructor(private modalCtrl:ModalController,public loadingCtrl: LoadingController,public voyage:VoyageProvider,public voiture:VoitureProvider,private storage: Storage,private platform: Platform,private geolocation: Geolocation,public variable: VarProvider,public navCtrl: NavController, public locationTracker: LocationTrackerProvider) {
     this.address = {
      place: ''
    };
  }  


  showAddressModal () {
    //Fix tout pourri car ionfocus event fires twice
    this.ionfocusfix++;
    if (this.ionfocusfix<2)
    {
      console.log("showadress modal function");
      let modal = this.modalCtrl.create(AutocompletePage);
      modal.onDidDismiss(data => {
        this.address.place = data;
        this.ionfocusfix=0;
      });
      modal.present();
      }
  }

   deleteetape(){
      let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").doc(this.variable.etape.id);
     return docRef.get()
     .then((data)=>{
         if(data.exists){
           this.variable.etape={
             id:"",
            nom:"",
            pos: new firebase.firestore.GeoPoint(0,0),
            update: firebase.firestore.Timestamp.now(),
            encours: false,
            duree:"Calcul en cours",
            distance:"Calcul en cours",
        	depart:new firebase.firestore.GeoPoint(0,0)
        };
           //On utilise set et non update car on veut une seule étape à chaque fois, on écrase le reste
           return docRef.update({
             encours : false
           });
         }else{
           console.log("Erreur, cela ne devrait jamais se produire. Dump docRef :",docRef);
         }

     }).catch((err)=>{console.log("Erreur lors de la suppression de letape :",err)})        
  }

  ajouterEtape(){
  	console.log("début fonction ajouterEtape");
    console.log("valeur this.address",this.address);
    //convert place to geocode
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': this.address.place }, (results, status) => {
      if (status === 'OK') {
    this.coordonnees.latitude = results[0].geometry.location.lat();
    this.coordonnees.longitude = results[0].geometry.location.lng();
    //Ajout dans firebase
     var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").doc();
     console.log("docRef =",docRef);
           let coordonnees=new firebase.firestore.GeoPoint(this.coordonnees.latitude,this.coordonnees.longitude);
           let timestampupdate=firebase.firestore.Timestamp.now();
           //On utilise set et non update car on veut une seule étape à chaque fois, on écrase le reste
           this.variable.etape.nom=this.address.place;
         this.variable.etape.pos=coordonnees;
         this.variable.etape.update=timestampupdate;
         this.variable.etape.encours=true;
         this.variable.etape.id=docRef.id;
           return docRef.set({
             pos : coordonnees,
             nom : this.address.place,
             update : timestampupdate,
             encours : true,
             depart : new firebase.firestore.GeoPoint(this.variable.pos.latitude,this.variable.pos.longitude)
     })
     .then((truc)=>{
           this.address.place="";
           window.alert("Etape ajoutée avec succès !");
        console.log("Fin fonction ajouterEtape");       
     })
     .catch((err)=>{console.log("Erreur lors de l'ajout de l'étape :",err)})

  }else{
    console.log("Une erreur est survenue lors de la conversion",status);
   }
   });
 }

	creerVoiture(nomvoiture) : void{
     this.voiture.creerVoiture(nomvoiture)
     .then((truc)=>{
       this.nomnouvellevoiture='';
     })
     .catch((err)=>{ console.log("Erreur ! ", err)})
   }


   ajouterAdmin(nom): Promise<any>{
     let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").where("nom","==",nom);
     return docRef.get()
     .then((data)=>{
       data.forEach((user)=>{
          let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").doc(user.id);
          return docRef.update({
            admin:true
          });
       });
     })
     .then((truc)=>{
       this.nomadmin="";
     })
     .catch((err)=>{console.log("Erreur lors de l'actualisation du statut admin :",err)})             
    }

 ionViewWillEnter(){
   //ugly fix pour event ionfocus qui fire deux fois
 this.ionfocusfix=0;
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminPage');
  }

}
