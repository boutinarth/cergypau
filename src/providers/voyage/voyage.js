var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
/*
  Provider avec toutes les fonctions de connexion, innitialisation etc etc
*/
var VoyageProvider = /** @class */ (function () {
    function VoyageProvider(voiture, storage, geolocation, variable, http, facebook) {
        this.voiture = voiture;
        this.storage = storage;
        this.geolocation = geolocation;
        this.variable = variable;
        this.http = http;
        this.facebook = facebook;
        this.db = firebase.firestore();
        console.log('Hello VoyageProvider Provider');
    }
    //Fonction qui retourne une promise avec comme valeur la liste des voyages dont l'utilisateur fait partie.
    VoyageProvider.prototype.loadVoyagesUser = function () {
        var voyageRef = this.db.collection('users').doc('iduser');
        return voyageRef.get();
    };
    //Récupère l'étape en cours depuis firebase et le stock dans variable.etape
    VoyageProvider.prototype.loadEtape = function () {
        var _this = this;
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").where("encours", "==", true).orderBy('update', 'desc').limit(1);
        return docRef.get()
            .then(function (data) {
            if (data.docs.length > 0) {
                var donnees = data.docs[0].data();
                _this.variable.etape.nom = donnees.nom;
                _this.variable.etape.pos = donnees.pos;
                _this.variable.etape.update = donnees.update;
                _this.variable.etape.encours = donnees.encours;
                _this.variable.etape.id = data.docs[0].id;
                return Promise.resolve(_this.variable.etape);
            }
            return Promise.reject("Pas d'étapes en cours");
        })
            .catch(function (err) { console.log("Erreur lors du chargement de létape en mémoire:", err); });
    };
    VoyageProvider.prototype.facebookLogin = function () {
        var _this = this;
        var docRef;
        var dbusername;
        var photourl;
        var userid;
        return this.geolocation.getCurrentPosition().then(function (resp) {
            return _this.position = new firebase.firestore.GeoPoint(resp.coords.latitude, resp.coords.longitude);
        }).then(function (machin) {
            return _this.facebook.login(['email']);
        }).then(function (response) {
            console.log("1 Firebase before auth ");
            var facebookCredential = firebase.auth.FacebookAuthProvider
                .credential(response.authResponse.accessToken);
            return firebase.auth().signInAndRetrieveDataWithCredential(facebookCredential);
        })
            .then(function (success) {
            console.log("2 Firebase success: " + JSON.stringify(success));
            //Si on arrive à se connecter on ajout l'user sur firestore
            //On stocke la valeur de l'id et du booléen LoginFb localement sur le téléphone.
            //Puis on redirige vers l'application
            userid = success.user.uid;
            dbusername = success.user.displayName;
            photourl = success.user.photoURL;
            console.log("3");
            docRef = _this.db.collection('users').doc(userid);
            //userid= docRef.id;
            return Promise.resolve(docRef);
        }).then(function (docref) {
            return Promise.resolve(docRef.get());
        }).then(function (snapshot) {
            console.log("Var snapshot", snapshot);
            console.log("Var variable ", _this.variable);
            if (!snapshot.exists) {
                docRef = _this.db.collection('users').doc(userid);
                return docRef.set({
                    name: dbusername,
                    pos: _this.position,
                    lastupdate: firebase.firestore.Timestamp.now(),
                    photo: photourl
                });
            }
            return Promise.resolve(true);
        }).catch(function (error) {
            console.log('Error getting location', error);
        })
            .then(function (truc) {
            console.log("4 variable truc", truc);
            _this.variable.loginName = dbusername;
            _this.variable.loginState = true;
            _this.variable.pos = _this.position;
            _this.variable.photoUrl = photourl;
            _this.variable.userid = userid;
            _this.variable.etape.duree = "Calcul en cours";
            _this.variable.etape.distance = "Calcul en cours";
            //TODO voyage handling
            // Hardcoded "ISa18qvwr1eicxDZV2Ag" : id du voyage du Cergy-Pau
            _this.variable.voyageid = "ISa18qvwr1eicxDZV2Ag";
            console.log("5 Var variable ", _this.variable);
            return Promise.all([_this.loadEtape(), _this.voiture.dbuserhasvoiture(), _this.voiture.getvoitures(), _this.storage.set('loginFb', true), _this.storage.set("userid", userid), _this.storage.set("voyageid", _this.variable.voyageid)]);
        })
            .then(function (data) {
            console.log("data promise 5", data);
        })
            .catch(function (error) { console.log("Firebase user exists error", error); });
    };
    VoyageProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [VoitureProvider, Storage, Geolocation, VarProvider, HttpClient, Facebook])
    ], VoyageProvider);
    return VoyageProvider;
}());
export { VoyageProvider };
//# sourceMappingURL=voyage.js.map