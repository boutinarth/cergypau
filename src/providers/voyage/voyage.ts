import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
import { Facebook } from '@ionic-native/facebook'
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
import { VoitureProvider } from '../../providers/voiture/voiture';
import {Observable} from 'rxjs/Observable';
import { BehaviorSubject } from "rxjs/Rx";

/*
  Provider avec toutes les fonctions de connexion, innitialisation etc etc
*/
@Injectable()
export class VoyageProvider {
	db = firebase.firestore();
	position : any;
  Etapenotifier : BehaviorSubject<any> = new BehaviorSubject(false);

  constructor(public voiture:VoitureProvider,public storage: Storage,private geolocation: Geolocation,public variable:VarProvider,public http: HttpClient,public facebook: Facebook) {
    console.log('Hello VoyageProvider Provider');
  }
  //Fonction qui retourne une promise avec comme valeur la liste des voyages dont l'utilisateur fait partie.
  loadVoyagesUser(): Promise<any>{
    let voyageRef = this.db.collection('users').doc('iduser');
    return voyageRef.get();
  }

  //Récupère l'étape en cours depuis firebase et le stock dans variable.etape
  loadEtapeListener(intervale : number):any{
    //On crée un observable qui fire toute les X secondes
    let observable = Observable.interval(intervale*1000);
    observable.subscribe((val)=>{
      let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").where("encours","==",true).orderBy('update','desc').limit(1);
       return docRef.get()
       .then((data)=>{
         if(data.docs.length>0){ 
           let donnees=data.docs[0].data();
           this.variable.etape.nom=donnees.nom;
           this.variable.etape.pos=donnees.pos;
           this.variable.etape.update=donnees.update;
           this.variable.etape.encours=donnees.encours;
           this.variable.etape.id=data.docs[0].id;
           this.variable.etape.depart=donnees.depart;
           this.loadDistanceEtape();
           this.Etapenotifier.next(true);
           return Promise.resolve(true);
         }else{
           this.variable.etape.encours=false;
         return Promise.resolve(false);
         }
       })
       .catch((err)=>{console.log("Erreur lors du chargement de létape en mémoire:",err)})
    })   
    return observable;          
  }

  loadEtapeNul():any{
    let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("etapes").where("encours","==",true).orderBy('update','desc').limit(1);
       return docRef.get()
       .then((data)=>{
         if(data.docs.length>0){ 
           let donnees=data.docs[0].data();
           this.variable.etape.nom=donnees.nom;
           this.variable.etape.pos=donnees.pos;
           this.variable.etape.update=donnees.update;
           this.variable.etape.encours=donnees.encours;
           this.variable.etape.id=data.docs[0].id;
           this.variable.etape.depart=donnees.depart;
           this.loadDistanceEtape();
           return Promise.resolve(true);
         }else{
           this.variable.etape.encours=false;
         return Promise.resolve(false);
         }
       })
       .catch((err)=>{console.log("Erreur lors du chargement de létape en mémoire:",err)})
  }

    loadDistanceEtape() {
    const idvoiture=this.variable.datavoiture.findIndex(i=> i.id === this.variable.voitureid);
    const voiture = this.variable.datavoiture[idvoiture];
    this.variable.etape.distance=voiture.etapeDistance;
    this.variable.etape.duree=voiture.etapeDuree;
  }

  facebookLogin(){
   var docRef;
   var dbusername;
   var photourl;
   var userid;
   //TODO voyage handling
   // Hardcoded "ISa18qvwr1eicxDZV2Ag" : id du voyage du Cergy-Pau
   var idCergyPau = "ISa18qvwr1eicxDZV2Ag";
    return this.geolocation.getCurrentPosition().then((resp)=>{
      return this.position=new firebase.firestore.GeoPoint(resp.coords.latitude,resp.coords.longitude);
    }).then((machin)=>{
  return this.facebook.login(['email'])
    }).then((response) => {
       console.log("1 Firebase before auth " ); 
      const facebookCredential = firebase.auth.FacebookAuthProvider
        .credential(response.authResponse.accessToken);

      return firebase.auth().signInAndRetrieveDataWithCredential(facebookCredential)
 		 })
        .then( success => { 
          console.log("2 Firebase success: " + JSON.stringify(success)); 
          //Si on arrive à se connecter on ajout l'user sur firestore
          //On stocke la valeur de l'id et du booléen LoginFb localement sur le téléphone.
          //Puis on redirige vers l'application
         userid = success.user.uid;
         dbusername=success.user.displayName;
         photourl=success.user.photoURL;
         console.log("3");
         docRef = this.db.collection('users').doc(userid);
        return Promise.resolve(docRef);
       }).then((docref)=>{
         return Promise.resolve(docRef.get());
       }).then((snapshot) => {
               console.log("Var snapshot",snapshot);
               console.log("Var variable ",this.variable);
               if(!snapshot.exists){
                   docRef = this.db.collection('users').doc(userid);
                   return docRef.set({
                   name: dbusername,
                   pos: this.position,
                   lastupdate: firebase.firestore.Timestamp.now(),
                   photo: photourl,
                   //voyage: idCergyPau assigne automatiquement au voyage
                   voyage: idCergyPau});
                 }else{
                 this.variable.posupdate=snapshot.data().lastupdate;
                 return Promise.resolve(true);
               }
                  }).catch((error) => {
                  console.log('Error getting location', error);
               })  
            .then((truc)=>{
            console.log("4 variable truc", truc);
            if(truc!=undefined){
              this.variable.loginName=dbusername;
              this.variable.loginState=true;
              this.variable.pos=this.position;
              this.variable.photoUrl=photourl;
              this.variable.userid=userid;
              this.variable.etape.duree="Calcul en cours";
              this.variable.etape.distance="Calcul en cours";
              //Assigne automatiquement au voyage
              this.variable.voyageid=idCergyPau;
              console.log("5 Var variable ",this.variable);
              //,this.voiture.getvoitures()
              return Promise.all([this.voiture.dbuserhasvoiture(),this.storage.set('loginFb',true),this.storage.set("userid",userid),this.storage.set("voyageid",this.variable.voyageid)]);
            }else{
              return Promise.reject("Erreur lors de la connexion");
            }
            })
            .then((data)=>{
              console.log("data promise 5",data);
            })           
           .catch((error) => { console.log("Erreur est survenue :",error) });
}
}
