import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';

/* Provider Variable qui sert à passer les variables globales dans l'application
*/
@Injectable()
export class VarProvider {
  public loginState:boolean = false;
  public aUneVoiture:boolean = false;
  public loginName:string = "";
  public pos: firebase.firestore.GeoPoint = new firebase.firestore.GeoPoint(0,0);
  public photoUrl: string;
  public voitureette: string;
  public voitureid: string;
  public voitureupdate: firebase.firestore.Timestamp = firebase.firestore.Timestamp.fromMillis(1);
  public datavoiture = new Array();
  public userid : string ="false";
  public voyageid : string ="false";
  public isadmin: boolean = false;
  public posupdate: firebase.firestore.Timestamp;
  public vitesse: number = 0.0;
  public polyline= new Array();
  public chat = new Array();
  public etape:{
    nom:string,
    pos:firebase.firestore.GeoPoint,
    update:firebase.firestore.Timestamp,
    encours:boolean,
    distance:string,
    duree:string,
    id:string,
    depart: firebase.firestore.GeoPoint
  };

  constructor(public storage : Storage) {
    console.log('Hello VariableProvider Provider');
    this.etape={
    id:"",
    nom:"",
    pos: new firebase.firestore.GeoPoint(0,0),
    update: firebase.firestore.Timestamp.now(),
    encours: false,
    distance:"",
    duree:"",
    depart:new firebase.firestore.GeoPoint(0,0)
  };
  }

}
