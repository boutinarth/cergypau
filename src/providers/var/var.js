var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
var VarProvider = /** @class */ (function () {
    function VarProvider(storage) {
        this.storage = storage;
        this.loginState = false;
        this.aUneVoiture = false;
        this.loginName = "";
        this.datavoiture = new Array();
        this.isadmin = false;
        console.log('Hello VariableProvider Provider');
        this.etape = {
            id: "",
            nom: "",
            pos: new firebase.firestore.GeoPoint(0, 0),
            update: firebase.firestore.Timestamp.now(),
            encours: false,
            distance: "",
            duree: ""
        };
    }
    VarProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Storage])
    ], VarProvider);
    return VarProvider;
}());
export { VarProvider };
//# sourceMappingURL=var.js.map