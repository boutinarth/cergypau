import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
import { Geolocation } from '@ionic-native/geolocation';
import { VoitureProvider } from '../../providers/voiture/voiture';
import { Message } from '../../interface/message';

/*
  Generated class for the ChatProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ChatProvider {
	db = firebase.firestore();
  unsubFunction : any;

  constructor(public voiture:VoitureProvider,private geolocation: Geolocation,public variable:VarProvider,public http: HttpClient) {
    console.log('Hello ChatProvider Provider');
  }

  //Listener Firebase pour la collection voitures
  listener(){
  	this.variable.chat=new Array<Message>();
  	let ref=this.db.collection("voyages").doc(this.variable.voyageid).collection("chat").orderBy("date").limit(30);
  	this.unsubFunction=ref.onSnapshot((array)=>{
  		 array.docChanges().forEach((change)=> {
  		 	let data=change.doc.data();
            if (change.type === "added") {
            	let message=data;
            	message.id=change.doc.id;
            	//Si un nouveau message arrive on l'ajoute
            	this.variable.chat.push(message)
                console.log("New message: ", JSON.stringify(data));
            }
            if (change.type === "modified") {
            	//Si un message est modifié on le modifie
            	//TODO low severity
                console.log("Message modifié: ", JSON.stringify(data));
            }
            if (change.type === "removed") {
            	//Si un message est supprimé on le retire
            	let index =this.variable.chat.findIndex(i=> i.id === change.doc.id);
            	this.variable.chat.splice(index,1);            	
                console.log("Message supprimé: ", JSON.stringify(data));
            }
        });
  	})
  }

  unsubscribe(){
    this.unsubFunction();
  }

  sendMessage(nouveau) : Promise<any>{
  let ref=this.db.collection("voyages").doc(this.variable.voyageid).collection("chat").doc();
  return ref.set({
    message:nouveau.message,
    nom:nouveau.nom,
    photo:nouveau.photo,
    date: firebase.firestore.Timestamp.now()
  });
}
	deleteMessage(id) : Promise<any>{
		let ref=this.db.collection("voyages").doc(this.variable.voyageid).collection("chat").doc(id);
		return ref.delete();
	}

}
