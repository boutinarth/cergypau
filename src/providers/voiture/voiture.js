var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
/*
  Generated class for the VoitureProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var VoitureProvider = /** @class */ (function () {
    function VoitureProvider(variable, http) {
        this.variable = variable;
        this.http = http;
        this.db = firebase.firestore();
        console.log('Hello VoitureProvider Provider');
    }
    VoitureProvider.prototype.dbgetvoiturebylocalid = function (id, tab) {
        for (var i = 0; i < tab.length; i++) {
            if (tab[i].id === id) {
                return tab[i];
            }
        }
    };
    //Fonction qui retourne une Promise après avoir affecté true ou false à this.aUneVoiture et l'id de la voiture à this.voiture si true.
    VoitureProvider.prototype.dbuserhasvoiture = function () {
        var _this = this;
        var retour = false;
        var colRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures");
        return colRef.get()
            .then(function (snapshot) {
            return Promise.all(snapshot.docs.map(function (row) {
                var colRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("voitures").doc(row.id).collection("users").doc(_this.variable.userid);
                return colRef.get()
                    .then(function (user) {
                    if (user.exists) {
                        _this.variable.aUneVoiture = true;
                        _this.variable.voitureid = row.id;
                        _this.variable.voitureette = _this.dbgetvoiturebylocalid(row.id, _this.variable.datavoiture);
                        console.log("L'user a une voiture, d'id :", row.id);
                    }
                    return Promise.resolve(user);
                });
            }));
        })
            .catch(function (err) {
            console.log('Error getting voyageid.userid.voitures collection', err);
        });
    };
    //Fonction qui retourne une Promise après avoir affecté la liste des voitures à this.datavoiture
    VoitureProvider.prototype.getvoitures = function () {
        var _this = this;
        this.variable.datavoiture = new Array();
        var colRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures");
        console.log("colRef = ", colRef);
        return colRef.get()
            .then(function (snapshot) {
            snapshot.forEach(function (doc) {
                var monjson = doc.data();
                monjson.id = doc.id;
                _this.variable.datavoiture.push(monjson);
            });
            return Promise.resolve(_this.variable.datavoiture);
        })
            .catch(function (err) {
            console.log('Error getting voiture collection', err);
        });
    };
    VoitureProvider.prototype.rejoindreVoiture = function (idvoiture) {
        var _this = this;
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(idvoiture).collection("users").doc(this.variable.userid);
        console.log("docref dans fonction rejoindrevoiture()", docRef);
        var setDoc = docRef.set({
            nom: this.variable.loginName,
            photo: this.variable.photoUrl,
            pos: this.variable.pos,
            lastupdate: firebase.firestore.Timestamp.now()
        });
        return setDoc
            .then(function (truc) {
            _this.variable.aUneVoiture = true;
            _this.variable.voitureid = idvoiture;
            console.log("Ajouté à la voiture", idvoiture, "l'utilisateur", _this.variable.loginName);
            return Promise.resolve(true);
        }).then(function (truc) {
            return Promise.resolve(_this.variable.voitureette = _this.dbgetvoiturebylocalid(idvoiture, _this.variable.datavoiture));
        }).catch(function (err) {
            console.log("Erreur lors de l'ajout a une voiture", err);
        });
    };
    VoitureProvider.prototype.creerVoiture = function (nomnouvellevoiture) {
        var _this = this;
        //on créee une reférence vers : Voiture.newdocumentid
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc();
        //on set le nom de la voiture, créant donc la structure dans firestore.
        var setDocRefMembresVoiture = docRef.set({
            nom: nomnouvellevoiture,
        }).then(function (truc) {
            //La voiture existant on refresh la liste des voitures
            return _this.getvoitures();
        });
    };
    VoitureProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [VarProvider, HttpClient])
    ], VoitureProvider);
    return VoitureProvider;
}());
export { VoitureProvider };
//# sourceMappingURL=voiture.js.map