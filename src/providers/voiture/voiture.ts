import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Voiture } from '../../interface/voiture';
import { VarProvider } from '../../providers/var/var';
import { BehaviorSubject } from "rxjs/Rx";
/*
  Generated class for the VoitureProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VoitureProvider {

  constructor(public variable:VarProvider,public http: HttpClient) {
    console.log('Hello VoitureProvider Provider');
  }

  db = firebase.firestore();
  notifier : BehaviorSubject<any> = new BehaviorSubject(false);

  dbgetvoiturebylocalid(id,tab){
    for (var i=0; i < tab.length; i++) {
        if (tab[i].id === id) {
            return tab[i];
              }
          }
      }

    //Fonction qui retourne une Promise après avoir affecté true ou false à this.aUneVoiture et l'id de la voiture à this.variable.voitureid si true.
  dbuserhasvoiture() : Promise<any>{
  	let retour = false;
    let colRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures");
    return colRef.get()
    .then((snapshot) => {
    	return Promise.all(snapshot.docs.map((row)=>{
    		let colRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(row.id).collection("users").doc(this.variable.userid);
    		return colRef.get()
    		.then((user) => {
		    			if(user.exists)
		    			{
		    				this.variable.aUneVoiture=true;
		    				this.variable.voitureid=row.id;
		    				this.variable.voitureette=row.data().nom;
		    				console.log("L'user a une voiture, d'id :",row.id);
		    			}
		    			return Promise.resolve(user);
    				});
		    }));    		
    	})
    .catch((err) => {
      console.log('Error getting voyageid.userid.voitures collection', err);
    });
  }


  //Fonction qui retourne une Promise après avoir affecté la liste des voitures à this.datavoiture
  getvoitures() : Promise<any>{
   	this.variable.datavoiture=new Array<Voiture>();
   	let colRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures");
   	console.log("colRef = ",colRef);
   	return colRef.get()
      .then((snapshot) => {
      	snapshot.forEach((doc) => {
        var monjson=doc.data();
        monjson.id=doc.id;
        this.variable.datavoiture.push(monjson);
      });
      return Promise.resolve(this.variable.datavoiture);
  })
    .catch((err) => {
      console.log('Error getting voiture collection', err);
    });
  }

  //Listener Firebase pour la collection voitures
  listenerColVoitures(){
  	this.variable.datavoiture=new Array<Voiture>();
  	const ref=this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").orderBy("etapeDistance");
  	return ref.onSnapshot((array)=>{
  		array.docChanges().forEach((change)=> {
  		 	let data=change.doc.data();
  		 	let voiture=data;
           	voiture.id=change.doc.id;
            if (change.type === "added") {
            	//Si une voiture est ajoutée
            	this.variable.datavoiture.push(voiture)
              this.notifier.next(voiture);
                console.log("Nouvelle voiture: ", JSON.stringify(data));
            }
            if (change.type === "modified") {
            	//Si une voiture est modifiée
            	let index =this.variable.datavoiture.findIndex(i=> i.id === change.doc.id);
            	this.variable.datavoiture[index]=voiture;
              this.notifier.next(voiture);
                console.log("Voiture modifiée: ", JSON.stringify(data));
                this.variable.datavoiture.sort(this.compareDistanceEtapeVoiture);
            }
            if (change.type === "removed") {
            	//Si une voiture est supprimée
            	let index =this.variable.datavoiture.findIndex(i=> i.id === change.doc.id);
            	this.variable.datavoiture.splice(index,1);            	
                console.log("Voiture supprimée: ", JSON.stringify(data));
            }
        });
  	})
  }

  rejoindreVoiture(idvoiture : string): Promise<any> {
     var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(idvoiture).collection("users").doc(this.variable.userid);
     console.log("docref dans fonction rejoindrevoiture()",docRef);
    return docRef.set({
         	admin:false,
         	nom: this.variable.loginName,
         	photo: this.variable.photoUrl,
         	pos: this.variable.pos,
         	lastupdate : firebase.firestore.Timestamp.now(),
          vitesse : 0.0
        })
     .then((truc)=>{
     	this.variable.aUneVoiture=true;  
     	this.variable.voitureid=idvoiture;
    	console.log("Ajouté à la voiture", idvoiture,"l'utilisateur",this.variable.loginName);
    	return Promise.resolve(true);
     }).then((truc)=>{
       return Promise.resolve(this.variable.voitureette=this.dbgetvoiturebylocalid(idvoiture,this.variable.datavoiture).nom);
     }).catch((err) =>{
      console.log("Erreur lors de l'ajout a une voiture",err);
    });
  }

  creerVoiture(nomnouvellevoiture: string) : Promise<any> {
     //on créee une reférence vers : Voiture.newdocumentid
     let docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc();
     //on set le nom de la voiture, créant donc la structure dans firestore.
     return docRef.set({
           nom: nomnouvellevoiture,
           etapeDistance : 99999
        });     
  }

  compareDistanceEtapeVoiture(voiturea : Voiture , voitureb : Voiture){
    return voiturea.etapeDistance - voitureb.etapeDistance;
  }
}
