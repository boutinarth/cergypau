var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
import { Geolocation } from '@ionic-native/geolocation';
/*
  Generated class for the LocationTrackerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LocationTrackerProvider = /** @class */ (function () {
    function LocationTrackerProvider(geolocation, variable, http) {
        this.geolocation = geolocation;
        this.variable = variable;
        this.http = http;
        this.db = firebase.firestore();
        console.log('Hello LocationTrackerProvider Provider');
    }
    LocationTrackerProvider.prototype.startTracking = function () {
        // Background Tracking
        var _this = this;
        this.subscription = this.geolocation.watchPosition().subscribe(function (position) {
            if (position.coords != undefined) {
                var docRef = _this.db.collection("voyages").doc(_this.variable.voyageid).collection("voitures").doc(_this.variable.voitureid).collection("users").doc(_this.variable.userid);
                var timenow = firebase.firestore.Timestamp.now();
                docRef.get().then(function (data) {
                    console.log("Changement de position détecté");
                    //30 = intervalle en secondes pour actualiser les données de la position dans la base de données (pour pas bouffer l'API firebase)
                    //A augmenter pour 100+ pour la prod TODO
                    return Promise.resolve(((timenow.seconds - data.data().lastupdate.seconds - 30) > 0));
                })
                    .then(function (doitupdate) {
                    if (doitupdate) {
                        var geoposition = position;
                        var posGeoPoint = new firebase.firestore.GeoPoint(geoposition.coords.latitude, geoposition.coords.longitude);
                        var setDoc = docRef.update({
                            lastupdate: timenow,
                            pos: posGeoPoint
                        }).catch(function (err) { console.log("Erreur lors de l'actualisation de la posisition", err); });
                        _this.variable.pos = posGeoPoint;
                        console.log('Position mise à jour : Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);
                    }
                }).catch(function (err) { console.log("Erreur lors du calcul des positions", err); });
            }
            else {
                console.log("Error lors de l'abonement à la position");
            }
        });
        // Turn ON the background-geolocation system.
        //this.backgroundGeolocation.start();
    };
    LocationTrackerProvider.prototype.stopTracking = function () {
        console.log('stopTracking');
        this.subscription.unsubscribe();
        //this.backgroundGeolocation.finish();
    };
    LocationTrackerProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Geolocation, VarProvider, HttpClient])
    ], LocationTrackerProvider);
    return LocationTrackerProvider;
}());
export { LocationTrackerProvider };
//# sourceMappingURL=location-tracker.js.map