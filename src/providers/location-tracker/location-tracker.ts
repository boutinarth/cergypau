import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import firebase from 'firebase';
import { VarProvider } from '../../providers/var/var';
import { Geolocation, PositionError, Geoposition } from '@ionic-native/geolocation';

/*
  Generated class for the LocationTrackerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationTrackerProvider {

  db = firebase.firestore();

  constructor(private geolocation: Geolocation,public variable:VarProvider, public http: HttpClient) {
    console.log('Hello LocationTrackerProvider Provider');
  }

  subscription : any;

  startTracking(interval : number) {
 
  // Background Tracking

	  this.subscription=this.geolocation.watchPosition().subscribe(position => {
    if ((position as Geoposition).coords != undefined) 
    {
        var docRef = this.db.collection("voyages").doc(this.variable.voyageid).collection("voitures").doc(this.variable.voitureid).collection("users").doc(this.variable.userid);
        var timenow = firebase.firestore.Timestamp.now();
            let vitesse=0.0;
              if(position.coords.speed!=null){
              vitesse=Math.floor(position.coords.speed*3.6);
              this.variable.vitesse=vitesse;
            }
          //console.log("Changement de position détecté");
          //parametre de la fonction : intervalle en secondes pour actualiser les données de la position dans la base de données (pour pas bouffer l'API firebase)
          //Mettre à environ 100 sec ou 200sec
        return Promise.resolve(((timenow.seconds-this.variable.posupdate.seconds-interval)>0))
        .then((doitupdate)=>{
            var geoposition = (position as Geoposition);   
            var posGeoPoint = new firebase.firestore.GeoPoint(geoposition.coords.latitude,geoposition.coords.longitude);
            this.variable.pos=posGeoPoint;
            if(doitupdate)
            {
              this.variable.posupdate=timenow;
             var setDoc = docRef.update({
               lastupdate: timenow,
               pos: posGeoPoint,
               vitesse: vitesse
            }).catch((err)=>{console.log("Erreur lors de l'actualisation de la posisition",err)});
             console.log('Position mise à jour : Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);
           }
        }).catch((err)=>{console.log("Erreur lors du calcul des positions",err)});	     
	    } else { 
	      console.log("Error lors de l'abonement à la position");
	    }
	});
	 
	  // Turn ON the background-geolocation system.
	  //this.backgroundGeolocation.start();
	}

stopTracking() {
 
  console.log('stopTracking');
 this.subscription.unsubscribe();
  //this.backgroundGeolocation.finish();
 
}

}
