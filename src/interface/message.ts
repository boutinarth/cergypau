export interface Message {
        message : string,
        nom : string,
        photo : string,
        date : {seconds:number, nanoseconds:number},
        id : string
    }
