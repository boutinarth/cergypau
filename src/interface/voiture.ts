export interface Voiture {
        etapeDistance : number,
        etapeDuree : string,
        nom : string,
        pos : {_lat : number,_long : number},
        update : {seconds:number, nanoseconds:number},
        vitesse : number,
        id : string
    }
