import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FiltreclassementvoiturePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filtreclassementvoiture',
})
export class FiltreclassementvoiturePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(voiture: any, ...args) {
  	if(voiture.etapeDistance<50000){
    	return voiture;
  	}
	}
}
