import { NgModule } from '@angular/core';
import { FiltreclassementvoiturePipe } from './filtreclassementvoiture/filtreclassementvoiture';
@NgModule({
	declarations: [FiltreclassementvoiturePipe],
	imports: [],
	exports: [FiltreclassementvoiturePipe]
})
export class PipesModule {}
