import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPageModule } from '../pages/login/login.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { VarProvider } from '../providers/var/var';
import { Facebook } from '@ionic-native/facebook'
import firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from './credentials';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from "@ionic-native/google-maps";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { NativeAudio } from '@ionic-native/native-audio';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';

import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { CartePage } from '../pages/carte/carte';
import { LoadingPage } from '../pages/loading/loading';
import { LoginPage } from '../pages/login/login';
import { AutocompletePage } from '../pages/autocomplete/autocomplete';
import { AdminPage } from '../pages/admin/admin'
import { ChatPage } from '../pages/chat/chat'


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { VoitureProvider } from '../providers/voiture/voiture';
import { VoyageProvider } from '../providers/voyage/voyage';
import { ChatProvider } from '../providers/chat/chat';
import { MemesPage } from '../pages/memes/memes';

firebase.initializeApp({ 
                         apiKey: "AIzaSyAN7SMAhml0uS5kVNQWrH4rPJ0dtIBvrRo",
                         authDomain: "cergypau.firebaseapp.com",
                         databaseURL: "https://cergypau.firebaseio.com",
                         projectId: "cergypau",
                         storageBucket: "cergypau.appspot.com",
                         messagingSenderId: "629242835630"
                       });

firebase.firestore().settings({timestampsInSnapshots: true});

@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    CartePage,
    LoadingPage,
    LoginPage,
    AutocompletePage,
    AdminPage,
    ChatPage,
    MemesPage
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    CartePage,
    LoadingPage,
    LoginPage,
    AutocompletePage,
    AdminPage,
    ChatPage,
    MemesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    VarProvider,
    Facebook,
    Geolocation,
    GoogleMaps,
    BackgroundGeolocation,
    LocationTrackerProvider,
    VoitureProvider,
    VoyageProvider,
    ChatProvider,
    InAppBrowser,
    YoutubeVideoPlayer,
    NativeAudio 
  ]
})
export class AppModule {}
