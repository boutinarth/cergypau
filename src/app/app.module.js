var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { VarProvider } from '../providers/var/var';
import { Facebook } from '@ionic-native/facebook';
import firebase from 'firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from './credentials';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from "@ionic-native/google-maps";
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { CartePage } from '../pages/carte/carte';
import { LoadingPage } from '../pages/loading/loading';
import { LoginPage } from '../pages/login/login';
import { AutocompletePage } from '../pages/autocomplete/autocomplete';
import { AdminPage } from '../pages/admin/admin';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BddProvider } from '../providers/bdd/bdd';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { VoitureProvider } from '../providers/voiture/voiture';
import { VoyageProvider } from '../providers/voyage/voyage';
firebase.initializeApp({
    apiKey: "AIzaSyAN7SMAhml0uS5kVNQWrH4rPJ0dtIBvrRo",
    authDomain: "cergypau.firebaseapp.com",
    databaseURL: "https://cergypau.firebaseio.com",
    projectId: "cergypau",
    storageBucket: "cergypau.appspot.com",
    messagingSenderId: "629242835630"
});
firebase.firestore().settings({ timestampsInSnapshots: true });
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                AboutPage,
                ContactPage,
                HomePage,
                TabsPage,
                CartePage,
                LoadingPage,
                LoginPage,
                AutocompletePage,
                AdminPage
            ],
            imports: [
                HttpClientModule,
                BrowserModule,
                IonicModule.forRoot(MyApp),
                IonicStorageModule.forRoot(),
                AngularFireModule.initializeApp(firebaseConfig),
                AngularFirestoreModule
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                AboutPage,
                ContactPage,
                HomePage,
                TabsPage,
                CartePage,
                LoadingPage,
                LoginPage,
                AutocompletePage,
                AdminPage
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                VarProvider,
                Facebook,
                BddProvider,
                Geolocation,
                GoogleMaps,
                BackgroundGeolocation,
                LocationTrackerProvider,
                VoitureProvider,
                VoyageProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map