import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();
admin.firestore().settings({timestampsInSnapshots: true});
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyDO3fdT8BgnPHcQKrJ1ertg_WKog_msAn8'
});

//Fonction qui met à jour la position et vitesse d'une voiture à chaque fois que la position d'un des utilisateurs est mise à jour.
//Et qui calcule également le temps restant jusqu'à l'étape pour la voiture.
export const updatePosVoiture = functions.firestore
  .document('voyages/{voyageid}/voitures/{idvoiture}/users/{userid}')
  .onUpdate(async (snap, context) => {
  	const newValue=snap.after.data();
  	const pos = newValue.pos;
  	const vitesse = newValue.vitesse;
  	const update = newValue.lastupdate;
  	const voyageid = context.params.voyageid;
  	const idvoiture = context.params.idvoiture;
  	const docEtape = admin.firestore().collection('voyages').doc(voyageid).collection('etapes').orderBy("update","desc").limit(1);
  	const docVoiture = admin.firestore().collection('voyages').doc(voyageid).collection('voitures').doc(idvoiture);
  	return docEtape.get()
        .then((data)=>{
          //ForEach ne s'effectue qu'une seule fois car on prend uniquement un résultat depuis firebase : limit(1)
        	data.forEach((row)=>{
        		const etape=row.data();
        		if(etape.encours){
        			  googleMapsClient.directions({
				      origin: pos.latitude+" "+pos.longitude,
				      destination: etape.pos.latitude+","+etape.pos.longitude
				    }, (truc,response) => {
				    	const status=response.json.status;
				    	//console.log("DEBUG : response :",response,"et status:",status);
				      if (status === 'OK') {
				        //console.log("DEBUG : RESPONSE GOOGLE DIRECTIONS :",response);
				        const infosTrajet=response.json.routes[0].legs[0];
				        const duree=infosTrajet.duration.text;
				        let distance=infosTrajet.distance.value;
				        distance= Math.round(distance/1000);
				        return docVoiture.update({
						  		pos:pos,
						  		update:update,
						  		etapeDistance:distance,
						  		etapeDuree:duree,
						  		vitesse:vitesse
					  		});
				    }else{
				    	return docVoiture.update({
						  		pos:pos,
						  		update:update,
						  		vitesse:vitesse 
					  		});
				    }
					});
        		}
        	})
        });
  });